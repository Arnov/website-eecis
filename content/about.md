+++
title = "Resume"
date = "2014-04-09"
+++

## Summary

Experienced Graduate with over 4 years of working experience in the Software Development and research role. Skilled in development/testing of software applications, and optimizing them by porting it to heterogeneous systems. Strong research professional with interests in High performance computing, Deep learning, Computer vision and Artificial Intelligence

## WORK EXPERIENCE

<b>University of Delaware, Newark, DE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;August 2016 - Present</b>
<br><i>Graduate Research Assistant</i>

* Researched on optimizing and parallelizing irregular and graph algorithms on GPUs, Multicore, ARM and power systems
* Supervised HPC cluster, CRPL Lab, University of Delaware as a system admin which is used by more than 15 people
* Researched on Deep Learning with TensorFlow, Caffe on Jetson TX2 and NVIDIA GPUs


<b>Infosys Ltd, Pune, India &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nov 2011 - Aug 2015</b>
<br><i>Test Engineer/ Developer</i>

* Developed and deployed modules in C++/Python for clients in Life sciences and publishing domain
* Developed and managed over 9 modules while leading team of 4 under me
* Created testing requirements document and conducted Certification Testing while supervising and coordinating with 25- person team; reduced system bugs by 29%
* Designed scripts for automation testing in QTP and tracked defects and maintained test cases in HP ALM
* Tested and traced defects and resolved bugs using agile and waterfall models
* Promoted to a Test Engineer for performing high-level testing

<b>ONGC, Mumbai, India &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;June 2010 - Aug 2010</b>
<br><i>System Security Intern</i>

* Devised scripts and protocols for secure cluster based on Linux
* Conducted penetration testing and vulnerability assessments, reducing SQL injection cases by 25% in 2 months


<b>PGS, Mumbai, India &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;June 2008 - Aug 2008</b>
<br><i>Systems Engineer Intern</i>

* Performed ETL activities in database for seismic data interpretation
* Developed an ERP system for the company, automating work environment in JAVA

Learn more and contribute on [GitHub](https://github.com/spf13).

## Programming skills

<b>Scripting</b><br>

* bash
* TCL
* Python

<b>Development</b><br>

* C/C++
* JAVA
* Python

<b>Testing</b><br>

* HP ALM
* QTP/UFT
* Manual Testing


<b>Parallel Programming</b><br>

* CUDA
* OpenMP
* OpenACC

<b>Database</b><br>

* MySQL
* PostgreSQL

<b>Others</b><br>

* OpenCV
* Deep learning
* SAP (FICO and ERP)
* Seibel CRM


## Education
<b>University of Delaware, Newark, DE</b><br>
<i>Masters of Science, Computer Science</i><br>
GPA: 3.6
<br><b>Thesis: </b>High Performance Sparse Fast Fourier Transform: Built an opensource parallel codebase for sparse FFT over GPUs & Multicore with OpenACC<br>

<b>University of Mumbai, Mumbai, India</b><br>
<i>Bachelor of Engineering, Computer Science</i><br>
GPA: 3.2

## Achievement

<b>Publication</b><br>
<i>Published</i><br>

* Sinha, Arnov (2014). [Rorschach based security on smartphones. Mumbai, India: IJSRD vol. 2 issue 7](http://www.academia.edu/15893970/Rorschach_Based_Security_for_Smartphones)

<i>Unpublished</i><br>

* Arnov Sinha, Dr. Sunita Chandrasekaran. “High Performance Sparse Fast Fourier Transform using OpenACC”. in: Unpublished
* Sinha, Arnov (2017b). “Sonar based image development”. In: Unpublished

<b>Award</b><br>

* Infosys, Ltd (2014). Best Quality Analyst Award
* University of Delaware (2017). Professional Development Award

<b>Talk</b><br>
* GTC 2017 Speaker on ["Using OpenACC to parallelize irregular computation"](http://on-demand.gputechconf.com/gtc/2017/presentation/s7478-arnov-sinha-using-openacc-to-parallelize-irregular-computation.pdf)

## projects

<b>Graduate Projects</b><br>

* Secure communication TCP
* Mars Rover simulator using Net Logo
* Image classification for Deep Learning
* Real time video Mosaic

<b>Undergraduate Projects</b><br>

* Opensource MPI library
* College webpage
* ERP system for PGS Ltd
* Game engine using Visual C

<b>Other Projects</b><br>

* Rorschach security for smartphones
* GPU sorting lib
* Sonar image development
* Python-OpenACC library

Download my resume [here](/Resume-Arnov-Sinha.pdf) or contact me regarding more details [here](mailto:arnov@udel.edu)</b><br>
Thanks for reading!
